#pragma once

#include "Includes.h"

class Smucar
{
private:
	int StartnaSt;
	std::string Priimek;
	int Cas;
public:
	Smucar();
	~Smucar();

	//Methods
	void NarediSmucarja(int startnaSt);
	void IzpisSmucarja();

	//Static methods
	static void VpisSmucarjev(int stVpisov);
	static std::vector<Smucar> PreberiSmucarje();
	static double Povprecje(std::vector<Smucar> smucarji);
	static void ShraniPovprecne(std::vector<Smucar> smucarji, double povprecje);
	static std::vector<Smucar> PreberiPovprecne();
	static void IzpisNajboljsega(std::vector<Smucar> smucarji);

	//Get/Set
	int getCas() const { return this->Cas; }
};

