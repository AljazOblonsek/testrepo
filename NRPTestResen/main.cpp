#include "Includes.h"
#include "Smucar.h"

int main() {

	int stVnosov;

	std::cout << "Koliko smucarjev zelis vpisati: ";
	std::cin >> stVnosov;
	std::cin.ignore();
	Smucar::VpisSmucarjev(stVnosov);

	std::vector<Smucar> vsiSmucarji = Smucar::PreberiSmucarje();
	std::cout << "--------------------------------" << std::endl;
	std::cout << "Povprecen cas: " << Smucar::Povprecje(vsiSmucarji) << std::endl;
	std::cout << "--------------------------------" << std::endl;

	Smucar::ShraniPovprecne(vsiSmucarji, Smucar::Povprecje(vsiSmucarji));
	std::vector<Smucar> najbolsiSmucarji = Smucar::PreberiPovprecne();

	for (Smucar smucar : najbolsiSmucarji)
		smucar.IzpisSmucarja();

	std::cout << "--------------------------------" << std::endl;
	std::cout << ".:Zmagovalec:." << std::endl;
	Smucar::IzpisNajboljsega(najbolsiSmucarji);
	std::cout << "--------------------------------" << std::endl;

	std::cin.get();
}