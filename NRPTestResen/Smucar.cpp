#include "Smucar.h"



Smucar::Smucar()
{
}


Smucar::~Smucar()
{
}

void Smucar::NarediSmucarja(int startnaSt)
{
	std::cout << "Vpisi priimek: ";
	std::getline(std::cin, this->Priimek);
	this->StartnaSt = startnaSt;
	this->Cas = rand() % 46 + 20 - 1;
}

void Smucar::IzpisSmucarja()
{
	std::cout << "Priimek: " << this->Priimek << std::endl;
	std::cout << "Startno stevilo: " << this->StartnaSt << std::endl;
	std::cout << "Cas: " << this->Cas << std::endl;
}

void Smucar::VpisSmucarjev(int stVpisov)
{
	Smucar tempSmucar;
	std::vector<Smucar> smucarji;
	std::fstream BinSmucarjiOut("smucarji.dat", std::ios::out | std::ios::binary | std::ios::trunc);

	for (int i = 0; i < stVpisov; i++) {
		tempSmucar.NarediSmucarja(i + 100);
		smucarji.push_back(tempSmucar);
	}

	BinSmucarjiOut.seekp(0, std::ios::beg);
	for (int i = 0; i < smucarji.size(); i++) {
		BinSmucarjiOut.write((char*)&smucarji[i], sizeof(Smucar));
	}

	BinSmucarjiOut.close();
}

std::vector<Smucar> Smucar::PreberiSmucarje()
{
	std::vector<Smucar> smucarji;
	std::fstream BinSmucarjiIn("smucarji.dat", std::ios::in | std::ios::binary);
	Smucar *tempSmucar;

	BinSmucarjiIn.seekg(0, std::ios::beg);
	while (!BinSmucarjiIn.eof()) {
		BinSmucarjiIn.read((char*)(tempSmucar = new Smucar()), sizeof(Smucar));

		if (BinSmucarjiIn.eof())
			break;

		smucarji.push_back(*tempSmucar);
	}

	BinSmucarjiIn.close();

	return smucarji;
}

double Smucar::Povprecje(std::vector<Smucar> smucarji)
{
	int vsota = 0;

	for (Smucar smucar : smucarji)
		vsota = vsota + smucar.getCas();

	return vsota / smucarji.size();
}

void Smucar::ShraniPovprecne(std::vector<Smucar> smucarji, double povprecje)
{
	std::fstream BinDobriSmucarjiOut("dobriSmucarji.dat", std::ios::out | std::ios::binary | std::ios::trunc);

	BinDobriSmucarjiOut.seekp(0, std::ios::beg);
	for (int i = 0; i < smucarji.size(); i++) {
		if (smucarji[i].getCas() < povprecje) {
			BinDobriSmucarjiOut.write((char*)&smucarji[i], sizeof(Smucar));
		}
	}

	BinDobriSmucarjiOut.close();
}

std::vector<Smucar> Smucar::PreberiPovprecne()
{
	std::vector<Smucar> smucarji;
	std::fstream BinDobriSmucarjiIn("dobriSmucarji.dat", std::ios::in | std::ios::binary);
	Smucar *tempSmucar;

	BinDobriSmucarjiIn.seekg(0, std::ios::beg);
	while (!BinDobriSmucarjiIn.eof()) {
		BinDobriSmucarjiIn.read((char*)(tempSmucar = new Smucar()), sizeof(Smucar));

		if (BinDobriSmucarjiIn.eof())
			break;

		smucarji.push_back(*tempSmucar);
	}

	BinDobriSmucarjiIn.close();

	return smucarji;
}

void Smucar::IzpisNajboljsega(std::vector<Smucar> smucarji)
{
	int najboljsiCas = INT_MAX;
	Smucar najboljsiSmucar;

	for (int i = 0; i < smucarji.size(); i++) {
		if (smucarji[i].getCas() < najboljsiCas) {
			najboljsiCas = smucarji[i].getCas();
			najboljsiSmucar = smucarji[i];
		}
	}

	najboljsiSmucar.IzpisSmucarja();
}